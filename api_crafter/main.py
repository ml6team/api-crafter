"""Script with logic to parse a OpenAPI yaml file to a code skeleton
"""
# pylint: disable=fixme
# pylint: disable=too-many-locals
import os
import logging
from collections import defaultdict
from shutil import rmtree
import argparse

from typing import List
import pkg_resources
import yaml
from jinja2 import Template
from autopep8 import fix_multiple_files, _get_options

STUB_PATH = pkg_resources.resource_filename('api_crafter', '__serverstub')


class ApiSpec:
    """Object to hold parsed spec and custom logic for template generation
    """

    def __init__(self, swagger):
        self.spec = swagger
        self._named_objects = self.parse_objects()

    @property
    def api_key(self):
        """Get API key, only if in query
        """
        key = self.spec.get('securityDefinitions', {}).get('api_key', [])
        if key and key['in'] == 'query':
            return key
        return None

    @property
    def api_title(self):
        """Get API title
        """
        return self.spec['info']['title']

    @property
    def security_types(self):
        """Get API security types
        """
        sec_types = set()
        security_defs = self.spec.get('securityDefinitions', {})
        # Check if there are securitySchemes specified (OAS 3).
        if not security_defs:
            security_defs = self.spec.get('components', {}).get('securitySchemes', {})
        for _, definition in security_defs.items():
            sec_types.add(definition.get('type', ''))
        return sec_types

    @property
    def named_objects(self):
        """Get all named objects defined in the spec with their defined methods
        """
        return self._named_objects

    def get_body_params(self, parms: List) -> dict:
        """Extract the body parameters from the spec.
        Handles two scenarios:
            - parameters specified in global definitions
            - parameters locally specified

        Args:
            parms: List with parameters of an operation

        Returns:
            dict: properties in body schema from the required input for operation
        """
        # get the schema if parameters are in body
        schema_list = [bp['schema'] for bp in parms if 'body' in bp.values()]

        # if schema exists, check if specified directly or referenced
        if schema_list:
            # extract schema from list
            schema = schema_list[0]

            # if schema referenced
            # format ref: [{'$ref': '#/definitions/__method__'}]
            if '$ref' in schema:
                # extract the path from the dict in a list.
                ref = schema['$ref']
                # split the path in sections
                ref = ref.split('/')
                # use the path to access the schema properties
                return self.spec[ref[1]][ref[2]]['properties']

            # if schema specified directly
            return schema['properties']
        # no params in body, return empty dict
        return {}

    def parse_objects(self):
        """Parse the API spec and extract all the objects to be created
        """
        # parse the spec to gather all paths and http verbs per OperationID
        # we can extract all classes and method names eg:
        # app.flask.routes.CLASS.METHOD
        named_objects = defaultdict(list)
        for path, pathspec in self.spec['paths'].items():
            path_parms = pathspec.get('parameters', [])
            for verb, verbspec in pathspec.items():
                if verb in ['get', 'post', 'put', 'delete']:
                    # get the class and method from the operationId
                    classname, method = verbspec['operationId'].split('.')[-2:]
                    # get parameters from verb
                    parms = verbspec.get('parameters', [])
                    method_spec = {
                        'name': method,
                        'verb': verb,
                        'path': path,
                        'description': verbspec['summary'],
                        'path_params': path_parms,
                        # generate correct params based on parameter location
                        # parameters in query
                        'query_params': [qp for qp in parms if 'query'
                                         in qp.values()],
                        # parameters in body
                        'body_params': self.get_body_params(parms),
                        # concat the specific parameters with the path
                        # parameters
                        'arguments': [arg['name'] for arg in path_parms
                                      + verbspec.get('parameters', [])],
                        # we only use the first response
                        'response_code': list(verbspec['responses'].keys())[0],
                        'response_message': list(verbspec['responses'].values())[0]['description'],
                    }

                    # Optionally add tags
                    if 'tags' in verbspec:
                        method_spec['tag'] = list(verbspec['tags'])[0]

                    named_objects[classname].append(method_spec)
        return named_objects

    def copy_tree(self, dest, force, source=STUB_PATH):
        """Copy over a template folder to the dest folder
            and fill in templates where needed using the provided spec
        Keyword Arguments:
            dest {str} -- [destination folder] (default: {'./api'})
            force {bool} -- [overwrite files]
            source {str} -- [template folder source] (default: {"./__serverstub"})
        """
        # TODO make this part of the class
        for root, dirs, files in os.walk(source, topdown=True):
            droot = root.replace(source, dest)

            # build the directories tree
            for directory in dirs:
                path = os.path.join(droot, directory)
                if os.path.exists(path) and not force:
                    print(f'File {path} exists already, skip generating')
                else:
                    rmtree(path, ignore_errors=True)
                    os.makedirs(path)

            # check for stubs to replace else copy file as is
            for file in files:
                file_prefix = os.path.splitext(file)[0].lower()
                file_suffix = os.path.splitext(file)[-1].lower()
                # TODO refactor duplicate logic
                if file_suffix == '.stub':
                    logging.info("found stub: %s", file)
                    # create new file based on template with .stub extension
                    # removed
                    stub_file = os.path.join(root, file)
                    out_file = os.path.join(droot, file.replace('.stub', ''))
                    print(f"generating: {out_file}")
                    with open(out_file, "w") as file_handle:
                        file_handle.write(self.destub(stub_file, self))

                elif file_suffix == '.xstub':
                    logging.info("found xstub: %s", file)
                    # xstub extensions are meant for creating multiple
                    # files based on a single template
                    # format: attribute.extension.xstub eg: titles.py.xstub
                    # the attribute will be used to fetch the correct data
                    # from the spec getattr(spec, attribute)

                    # every .xstub file has a .context file specifing
                    # the naming of the cascading files
                    context_file = os.path.join(root, file_prefix + ".context")
                    with open(context_file) as file_handle:
                        context = yaml.safe_load(file_handle)

                    stub_file = os.path.join(root, file)
                    for name, methods in getattr(
                            self, context['target']).items():
                        cascade_file = self.resolve_context(
                            name.lower(), context)
                        out_file = os.path.join(
                            droot, cascade_file)
                        print(f"generating: {out_file}")
                        with open(out_file, "w") as file_handle:
                            file_handle.write(
                                self.destub(stub_file, {'name': name, 'methods': methods}))
                elif file_suffix in ['.context', '.pyc']:
                    # .context files are processed with the .xstub files
                    continue
                else:
                    print(f"generating: {file}")
                    with open(os.path.join(droot, file), "w") as file_handle:
                        file_handle.write(self.destub(
                            os.path.join(root, file), self))

    def destub(self, file, params):  # pylint: disable=no-self-use
        """Read file to jinja template and render with the given spec
        """
        with open(file) as template_raw:
            template = Template(template_raw.read())
        return template.render(spec=params)

    def resolve_context(self, name, context):  # pylint: disable=no-self-use
        """Build the name of the file based on the conext parameters
             file_prefix + name + file_suffix + file_type
        Returns:
            [str] -- full file name
        """
        if context['file_prefix']:  # walrus operator would be nice
            name = context['file_prefix'] + name
        if context['file_suffix']:
            name += context['file_suffix']
        if context['file_type']:
            name += context['file_type']
        return name


def craft(swagger, output, *, force, no_lint, verbose):
    """Run API crafting.

        Args:
            swagger (dict): swagger spec
            output (str): output folder for the generated API
            force (bool): either to overwrite existing files while generating API
            no_lint (bool): either to apply automatic PEP8 linting
            verbose (bool): verbose mode

    """
    if verbose:
        logging.basicConfig(level=logging.INFO)

    # parse API spec
    api_spec = ApiSpec(swagger=swagger)

    # generate code
    api_spec.copy_tree(dest=output, force=force)

    # dump spec to configs folder
    config_path = os.path.join(output, 'configs')
    if not os.path.exists(config_path):
        os.makedirs(config_path)
    yaml.dump(swagger,
              open(os.path.join(config_path, 'swagger.yaml'), 'w'),
              default_flow_style=False,
              sort_keys=False)

    if not no_lint:
        # run autopep8 on generated code
        autopep8_args = {
            'in_place': True,
            'recursive': True,
            'aggressive': 2,
            'verbose': verbose
        }
        autopep8_options = _get_options(autopep8_args, apply_config=False)
        fix_multiple_files([output], options=autopep8_options)


def main():
    """Entrypoint function."""
    parser = argparse.ArgumentParser(
        description='Code Generator for OpenAPI specs')
    parser.add_argument('input',
                        help='input OpenAPI spec yaml')
    parser.add_argument('-o', '--output',
                        help='output folder for the generated code',
                        default='./api')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='verbose flag, generates more output',
                        default=False)
    parser.add_argument('-f', '--force',
                        action='store_true',
                        help='overwrite file or folder if already exists',
                        default=False)
    parser.add_argument('--no-lint',
                        action='store_true',
                        help='do not automatically lint the code after '
                             'generation (PEP8)')
    args = parser.parse_args()

    swagger = yaml.safe_load(open(args.input))
    craft(swagger, args.output, force=args.force,
          no_lint=args.no_lint, verbose=args.verbose)
