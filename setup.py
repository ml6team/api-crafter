import setuptools

with open('README.md', 'r') as f:
    long_description = f.read()

requirements = [
    'autopep8>=1.4.4',
    'Jinja2>=2.10.3',
    'MarkupSafe>=1.1.1',
    'pycodestyle>=2.5.0',
    'autopep8>=1.4.4',
    'PyYAML>=5.2'
]

dev_requirements = [
    'pylint==2.4.4',
    'bandit==1.6.2',
    'liccheck==0.9.2'
]

setuptools.setup(
    name='api-crafter',
    version='0.0.1',
    author='ML6',
    description='Tool to generate a Connexion service from a swagger.yaml',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    install_requires=requirements,
    extras_require={
        'dev': dev_requirements
    },
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'api-crafter = api_crafter.main:main',
        ],
    },
    python_requires='>=3.6'
)
