# API-CRAFTER

Converts OpenAPI(swagger) specs to a working flask skeleton to jumpstart your microservice development.


## Features

- Fully working flask application
- Conform the ML6 coding guidelines
- Conform Pylint and PEP8 coding styles
- Same structure as the microservice boilerplate
- Automatic test generation and test setup


## Installation

You can install the api-crafter using

```
# SSH
pip install git+ssh://git@bitbucket.org/ml6team/api-crafter.git

# HTTPS
pip install git+https://bitbucket.org/ml6team/api-crafter.git
```

To install from github instead, simply replace 'bitbucket.org' by 'github.com' in the previous
commands.

## For Api-Crafter users

Firstly you will need a valid swagger spec (version 2.0). 
You can use any of the following tools to create one:

- [the online editor](https://editor.swagger.io/)
- [the pycharm plugin by Zalando](https://plugins.jetbrains.com/plugin/8347-swagger)
- [The VS code plugin](https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi)

Please follow the [REST(FUL) API guidelines](https://sites.google.com/skyhaus.com/intra/delivery/coding-guidelines/general-coding-guidelines#h.p_vwqPW-NdwVW3) 
for proper RESTful API design.

After you have obtained a swagger spec in yaml format you can run the Api-Crafter by running the following command:
```
api-crafter YOURYAML.yaml
```

You can specify the output folder for the API by providing `-o ./YOURFOLDER` 
flag. By default the API is generated in the `./api` folder.

You can get more info about the usage by using the help flag:
```
api-crafter --help
```

You will now have a folder containing a working flask application!

To start the flask app create a fresh (python3) virtual environment and run the following commands:
```
cd ./YOURFOLDER
pip install - r requirements.txt
python run.py
```

You should now be able to visit the hosted documentation (```http://0.0.0.0:8080/ui/```)!

You can run the generated tests:
```
pip install .[dev]
cd ./YOURFOLDER
python -m pytest .
```

Note: The tests will fail out of the box because the generated code will raise ``` NotImplementedError ```'s. You can start implementing your logic until the test passes.

## For Api-Crafter developers

The main idea is based on the server generators in the [swagger editor](https://editor.swagger.io)
([Repo in java](https://github.com/swagger-api/swagger-codegen))
There is also a python example (really different folder structure).

This version (using pure python) should generate the code structure used in the [Pets example](https://bitbucket.org/ml6team/microservice-boilerplate/src/master/examples/pets/)
from the microservice boilerplate given a swagger yaml.

We use an existing file structure (/__serverstub) which is just a copy of the Pets example
code with some files edited as .stub or .xstub which then get filled in using jinja templating.
We parse the spec to a python object that exposes certain properties that can be consumed
by the jinja templating. After generating the code the code is automatically linted using autopep8.
The flask server is operational after generating but
every function in the core space will raise not implemented error's.

Contributions are greatly appreciated! For contributing please check the 
[Bitbucket issue tracker](https://bitbucket.org/ml6team/api-crafter/issues?status=new&status=open) 
or contact georges.lorre@ml6.eu.

To install a development version of api-crafter in editable mode run on of these two command based
on your terminal version:
```
pip install -e .[dev]
```
```
pip install -e '.[dev]'
```
